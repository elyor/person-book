package model;

/**
 * Created by IntelliJ IDEA.
 * User: RTulyaganov
 * Date: 8/8/12
 * Time: 11:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class Person {
    private Long id;
    private String lastName;
    private String firstName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}

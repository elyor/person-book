package ui;

import dao.ObjectNotFoundException;
import dao.PersonDao;
import model.Person;

import javax.swing.*;
import java.sql.SQLException;
import java.util.List;

import static java.lang.System.*;

/**
 * Created by IntelliJ IDEA.
 * User: RTulyaganov
 * Date: 8/8/12
 * Time: 11:17 PM
 * To change this template use File | Settings | File Templates.
 */

   @SuppressWarnings("UnnecessaryLocalVariable")
   public class PersonTableGui
   {
       private PersonDao personDao;
       private JTable personTable;

       public PersonDao getPersonDao() {
           return personDao;
       }

       public PersonTableGui(PersonDao personDao) throws Exception {
           this.personDao = personDao;
        }

        public JTable runPersonTable(Person person) throws Exception {
            personTable = new JTable(rowData(person), columnNames()) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            return personTable;
        }
        //with Text
        private Object[][] rowData(Person person) {
            String[][] data = null ;
            List<Person> listPersons;
            try{
                listPersons = getPersonDao().getPersons(person);
                data = new String[listPersons.size()][3];
                String[] row = new String[3];
                for(int i=0; i<listPersons.size(); i++){
                    person = new Person();
                    person = listPersons.get(i);
                    row[0] = person.getId().toString();
                    row[1] = person.getLastName();
                    row[2] = person.getFirstName();
                    arraycopy(row, 0, data[i], 0, 3);
                }
            }
            catch(Exception ex){
                err.println(ex + " I am here");
            }
            return data;
        }

        public JTable runPersonTable(Long id) throws SQLException, ObjectNotFoundException {
            personTable = new JTable(rowData(id), columnNames()) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            return personTable;
        }
        private Object[][] rowData(Long id) {
            String[][] data = null;
            List<Person> listPersons;
            try{
                listPersons = getPersonDao().getPerson(id);
                data = new String[listPersons.size()][3];
                String[] row = new String[3];
                for(int i = 0; i < listPersons.size(); i ++) {
                    Person person;
                    person = listPersons.get(i);
                    row[0] = person.getId().toString();
                    row[1] = person.getLastName();
                    row[2] = person.getFirstName();
                    arraycopy(row, 0, data[i], 0, 3);
                }
            } catch(Exception ex) {
                err.println(ex);
            }
            return data;
        }
        private String[] columnNames() {
            String[] columns = {"ID","Last Name","First Name"};
            return columns;
        }


   }




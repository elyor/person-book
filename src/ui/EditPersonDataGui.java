package ui;

import dao.PersonDao;
import model.Person;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: RTulyaganov
 * Date: 8/8/12
 * Time: 11:17 PM
 * To change this template use File | Settings | File Templates.
 */

   public class EditPersonDataGui extends JDialog implements ActionListener {
    /* Frame instance */
        JDialog editPersonDataFrame = new JDialog(this,"Edit Person Data",true);
        private JButton addPersonButton, cancelCommendButton, savePersonData, deletePersonButton;
        private JTextField personIdField, personLastName, personFirstName;
        private JLabel personFirstNameLabel;
        private Person person = new Person();
        private PersonDao personDao;

        public PersonDao getPersonDao() {
            return personDao;
        }

    public EditPersonDataGui(Long personId, PersonDao personDao) {
        this.personDao = personDao;
            /* Frame components  */
            editPersonDataFrame.setVisible(false);
            editPersonDataFrame.setSize(350, 250);
            editPersonDataFrame.setLocationRelativeTo(null);
            editPersonDataFrame.setResizable(false);

            getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                    KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE , 0), "close");
            getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                    KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "close");
            getRootPane().getActionMap().put("close", new AbstractAction() {
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });

            personIdField = new JTextField(5);
            personIdField.setVisible(false);

            JLabel personIdLabel = new JLabel("Person Id");
            personIdLabel.setVisible(false);

            JLabel personLastNameLabel = new JLabel("Last Name");
            personLastName = new JTextField(20);
            personFirstName = new JTextField(20);
            personFirstNameLabel = new JLabel("First Name");

            addPersonButton = new JButton("Add");
            addPersonButton.addActionListener(this);

            savePersonData  = new JButton("Save");
            savePersonData.setVisible(false);

            deletePersonButton = new JButton("Delete");
            deletePersonButton.setVisible(false);

            cancelCommendButton = new JButton("Cancel");
            cancelCommendButton.addActionListener(this);

/*GridGabLayout */

            JPanel panel = new JPanel(new GridBagLayout());
            GridBagConstraints constanta = new GridBagConstraints();
            editPersonDataFrame.getContentPane().add(panel,BorderLayout.NORTH);

            constanta.gridx = 0;
            constanta.gridy = 1;
            constanta.fill = GridBagConstraints.HORIZONTAL;
            constanta.insets = new Insets(20, 10, 10, 10);
            panel.add(personIdLabel, constanta);

            constanta.gridx = GridBagConstraints.RELATIVE;
            constanta.gridy = 1;
            panel.add(personIdField, constanta);

            constanta.gridx = 0;
            constanta.gridy = 2;
            panel.add(personLastNameLabel,constanta);

            constanta.gridx = GridBagConstraints.RELATIVE;
            constanta.gridy = 2;
            panel.add(personLastName, constanta);

            constanta.gridx = 0;
            constanta.gridy = 3;
            panel.add(personFirstNameLabel, constanta);

            constanta.gridx = GridBagConstraints.RELATIVE;
            constanta.gridy = 3;
            panel.add(personFirstName, constanta);

            /* New Panel for save,add, cancel buttons */
            JPanel panel2 = new JPanel(new GridBagLayout());
            editPersonDataFrame.add(panel2);
            GridBagConstraints con = new GridBagConstraints();

            con.insets = new Insets(10, 15, 5, 5);
            panel2.add(savePersonData, con);
            panel2.add(addPersonButton, con);
            panel2.add(deletePersonButton, con);

            panel2.add(cancelCommendButton, con);
            con.anchor = GridBagConstraints.EAST;

            if(personId != null) {
                try{
                    person = getPersonDao().getPersonById(personId);
                    System.out.println(person.getFirstName());
                    personIdField.setVisible(true);
                    personIdField.setText(person.getId().toString());
                    personIdField.setEditable(false);

                    personLastName.setEditable(true);
                    personFirstName.setEditable(true);

                    personLastName.setText(person.getLastName());
                    personFirstName.setText(person.getFirstName());

                    deletePersonButton.setVisible(true);
                    deletePersonButton.addActionListener(this);

                    savePersonData.setVisible(true);
                    savePersonData.addActionListener(this);

                    addPersonButton.setVisible(false);
                    personIdLabel.setVisible(true);
                }
                catch(Exception sql) {
                    JOptionPane.showInternalMessageDialog(null, sql);
                }
            }
        }

/*
* Action performed method
* Here, add, delete or cancel comments events are handled
*/

        public void actionPerformed(ActionEvent event) {
            if(event.getSource() == addPersonButton) {
                try{
                    String lastNameText = personLastName.getText();   //reading input into String variable
                    String firstNameText = personFirstName.getText();  //reading input into String variable

                    if(!lastNameText.isEmpty() && !firstNameText.isEmpty()){
                        person.setLastName(lastNameText);
                        person.setFirstName(firstNameText);
                        getPersonDao().addPerson(person);
                        editPersonDataFrame.setVisible(false);
                    }
                    else {
                        JOptionPane.showMessageDialog(null,  "All Fields should be filled out");
                    }
                } catch(SQLException sql)
                {
                    JOptionPane.showMessageDialog(null,  sql);
                    System.err.println(sql);
                }
            }
            else if(event.getSource() == cancelCommendButton) {
                editPersonDataFrame.setVisible(false);
            }
            else if(event.getSource() == savePersonData) {
                try{
                    Long id =  Long.parseLong(personIdField.getText());
                    String lastNameText = personLastName.getText();
                    String firstNameText = personFirstName.getText();
                    person.setId(id);
                    person.setLastName(lastNameText);
                    person.setFirstName(firstNameText);
                    getPersonDao().updatePerson(person);
                    editPersonDataFrame.setVisible(false);
                } catch(SQLException sql)
                {
                    JOptionPane.showMessageDialog(null,  sql);
                    System.err.println(sql);
                }
            }else if(event.getSource() == deletePersonButton) {
                Long id = Long.parseLong(personIdField.getText());
                int confirm = JOptionPane.showConfirmDialog(editPersonDataFrame, "Are you sure to delete data","Delete", JOptionPane.YES_OPTION);
                if(confirm == 0) {
                    try{
                        getPersonDao().deletePerson(id);
                        editPersonDataFrame.setVisible(false);
                    }catch(SQLException ex) {
                        JOptionPane.showMessageDialog(null, ex);
                    }
                }
            }
        }



}







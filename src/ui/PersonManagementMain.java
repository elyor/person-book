package ui;

import dao.PersonDao;
import model.Person;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Created by IntelliJ IDEA.
 * User: RTulyaganov
 * Date: 8/8/12
 * Time: 11:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class PersonManagementMain {

    public static void main(String args[]) {
        try{
            PersonDao personDao = new PersonDao();
            java.lang.Long id = null;
            Person person = new Person();

            PersonTableGui personTableGui = new PersonTableGui(personDao);
            EditPersonDataGui editPersonDataGui = new EditPersonDataGui(id, personDao);

            final PersonFrame gui = new PersonFrame(personDao, person, personTableGui);
            gui.setVisible(true);

            personTableGui.runPersonTable(person);
            editPersonDataGui.setVisible(false);

            WindowListener l = new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e){
                    int confirm = JOptionPane.showOptionDialog(gui, "Do you want to exit?","Quit?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
                    if(confirm==0){
                        gui.dispose();
                        System.exit(confirm);
                    }else
                        gui.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                }
            };
            gui.addWindowListener(l);
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, String.format("The error is %d", e.getMessage()));
        }
    }
}

package ui;

import dao.ObjectNotFoundException;
import dao.PersonDao;
import model.Person;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: RTulyaganov
 * Date: 8/8/12
 * Time: 11:16 PM
 * To change this template use File | Settings | File Templates.
 */
    public class PersonFrame extends JFrame {
        protected JTextField personId, personLastName, personFirstName;
        protected JButton getPersonById, addPersonButton, selectAllPersonsButton;
        protected JTable personTable;
        protected JScrollPane scrollers;
        protected JPanel panel, panelCenter;
        protected GridBagConstraints constata;
        protected JLabel personIdLabel, personLastNameLabel, personFirstNameLabel;
        final Mouse mouseClicked = new Mouse();

        private Person person;
        private PersonDao personDao;
        private PersonTableGui personTableGui;

        public PersonFrame(PersonDao personDao, final Person person, PersonTableGui personTableGui) {
            super("Person data GUI");
            setSize(1100,700);
            setLocationRelativeTo(null);
            this.personDao = personDao;
            this.person = person;
            this.personTableGui = personTableGui;

            /* North panel */
            panel = new JPanel(new java.awt.GridBagLayout());
            add(panel, BorderLayout.NORTH);
            /* Center panel */
            panelCenter = new JPanel(new java.awt.GridBagLayout());
            add(panelCenter, BorderLayout.CENTER);
            constata = new GridBagConstraints();

            /* Handler class part */
            HandlerClass handler = new HandlerClass();
            KeyHandler keyHandler = new KeyHandler();
            /* Handler class part ends */

            /* Components are created and added here */
            personIdLabel = new JLabel("ID");
            personIdLabel.setFont(new Font("Serif", Font.PLAIN, 16));
            constata.gridx = 0;
            constata.gridy = 0;
            constata.insets = new Insets(20, 70, 20, 3);
            constata.anchor = GridBagConstraints.FIRST_LINE_START;
            panel.add(personIdLabel, constata);

            personId = new JTextField(5);
            personId.setEditable(true);
            personId.setFont(  new Font("Serif", Font.PLAIN, 16));
            personId.setToolTipText("ID field of a person");
            constata.gridx = 1;
            constata.gridy = 0;
            constata.insets = new Insets(20, 0, 20, 10);
            panel.add(personId, constata);
            personId.addKeyListener(keyHandler);

            personLastNameLabel = new JLabel("LAST NAME");
            personLastNameLabel.setFont(new Font("Serif", Font.PLAIN, 16));
            constata.gridx = 2;
            constata.gridy = 0;
            constata.insets = new Insets(20, 15, 20, 3);
            panel.add(personLastNameLabel, constata);

            personLastName = new JTextField(15);
            personLastName.setFont(new Font("Serif", Font.PLAIN, 16));
            personLastName.setEditable(true);
            personLastName.setToolTipText("Last name of a person");
            personLastName.addActionListener(handler);
            constata.gridx = 3;
            constata.gridy = 0;
            constata.insets = new Insets(20, 0, 20, 10);
            panel.add(personLastName, constata);

            personFirstNameLabel = new JLabel("First Name");
            personFirstNameLabel.setFont( new Font("Serfi", Font.PLAIN, 16));
            constata.gridx = 4;
            constata.gridy = 0;
            constata.insets = new Insets(20, 15, 20, 3);
            panel.add(personFirstNameLabel,constata);

            personFirstName = new JTextField(15);
            personFirstName.setFont( new Font("Serif", Font.PLAIN, 16));
            personFirstName.setToolTipText("First name of a person");
            personFirstName.addActionListener(handler);
            constata.gridx = 5;
            constata.gridy = 0;
            constata.insets = new Insets(20, 0, 20, 10);
            panel.add(personFirstName, constata);

            getPersonById = new JButton("Filter");
            getPersonById.setToolTipText("Press Find button to search a person by given ID");
            constata.gridx = 6;
            constata.gridy = 0;
            constata.insets = new Insets(20, 10, 20, 10);
            panel.add(getPersonById, constata);
            getPersonById.addActionListener(handler);

            selectAllPersonsButton = new JButton("Reset");
            selectAllPersonsButton.setToolTipText("Press select button to retrieve base to refresh the table");
            constata.gridx = 7;
            constata.gridy = 0;
            panel.add(selectAllPersonsButton, constata);
            selectAllPersonsButton.addActionListener(
                    new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            try {
                                panelCenter.remove(scrollers);
                                personLastName.setText(null);
                                personFirstName.setText(null);
                                personId.setText(null);
                                person.setLastName(personLastName.getText());
                                person.setFirstName(personFirstName.getText());

                                personTable = getPersonTable(person);

                                personTable.addMouseListener(mouseClicked);
                                scrollers = new JScrollPane(personTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                        JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                                constata.gridx = 3;
                                constata.gridy = 2;
                                constata.fill = GridBagConstraints.BOTH;
                                constata.insets = new Insets(10, 70, 20, 70);
                                constata.weightx = 10;
                                constata.weighty = 10;
                                panelCenter.add(scrollers,constata);
                                panelCenter.revalidate();
                                panelCenter.repaint();
                            } catch (Exception ex) {
                                System.err.println(ex);
                            }
                        }
                    }
            );

            addPersonButton = new JButton("Add");
            addPersonButton.setToolTipText("Press add button to insert new person data");
            addPersonButton.addActionListener(handler);
            constata.gridx = 8;
            constata.gridy = 0;
            constata.weightx = 1;
            panel.add(addPersonButton, constata);

            person.setLastName(personLastName.getText());
            person.setFirstName(personFirstName.getText());
            personTable = getPersonTable(person);
            personTable.addMouseListener(mouseClicked);
            constata.gridx = 3;
            constata.gridy = 2;
            constata.fill = GridBagConstraints.BOTH;
            constata.insets = new Insets(10, 70, 20, 70);
            constata.weightx = 10;
            constata.weighty = 10;
            scrollers = new JScrollPane (personTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            panelCenter.add(scrollers, constata);
        }

    private JTable getPersonTable(Person person) {
            try {
                personTable = personTableGui.runPersonTable(person);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(getContentPane(), "Exception occurred");
                Logger.getLogger(PersonFrame.class.getName()).log(Level.SEVERE, null, e);
            }
            return personTable;
        }

        private JTable getPersonTable(Long id) {
            try {
                personTable = personTableGui.runPersonTable(id);
            } catch (SQLException sql) {
                JOptionPane.showMessageDialog(getContentPane(), "Sql error occurred while method running");
                Logger.getLogger(PersonFrame.class.getName()).log(Level.SEVERE, null, sql);
                personId.setText(null);
            } catch (ObjectNotFoundException obj) {
                JOptionPane.showMessageDialog(getContentPane(), "Object not found");
                Logger.getLogger(PersonFrame.class.getName()).log(Level.SEVERE, null, obj);
            }
            return personTable;
        }
        /*
        * Mouse listener method
        */
        private class Mouse extends MouseAdapter {
            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                if(mouseEvent.getClickCount() == 2) {
                    int row = personTable.getSelectedRow();
                    String rowNew  = (String) personTable.getValueAt(row, 0);
                    Long id = Long.parseLong(rowNew);
                    EditPersonDataGui edit = new EditPersonDataGui(id,  personDao);
                    edit.editPersonDataFrame.setVisible(true);

                    if(!personId.getText().isEmpty()) {
                        panelCenter.remove(scrollers);
                        personTable = getPersonTable(id);
                        personTable.addMouseListener(mouseClicked);
                        scrollers = new JScrollPane (personTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                        constata.gridx = 3;
                        constata.gridy = 2;
                        constata.fill = GridBagConstraints.BOTH;
                        constata.insets = new Insets(10, 70, 20, 70);
                        constata.weightx = 10;
                        constata.weighty = 10;
                        panelCenter.add(scrollers, constata);
                        panelCenter.revalidate();
                        panelCenter.repaint();
                    }
                    else if(!personLastName.getText().isEmpty() || !personLastName.getText().isEmpty() && !personFirstName.getText().isEmpty() || !personFirstName.getText().isEmpty()) {
                        panelCenter.remove(scrollers);
                        person.setLastName(personLastName.getText());
                        person.setFirstName(personFirstName.getText());

                        personTable = getPersonTable(person);
                        personTable.addMouseListener(mouseClicked);
                        scrollers = new JScrollPane (personTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                        constata.gridx = 3;
                        constata.gridy = 2;
                        constata.fill = GridBagConstraints.BOTH;
                        constata.insets = new Insets(10, 70, 20, 70);
                        constata.weightx = 10;
                        constata.weighty = 10;

                        panelCenter.add(scrollers,constata);
                        panelCenter.revalidate();
                        panelCenter.repaint();
                    }
                    else {
                        panelCenter.remove(scrollers);
                        person.setLastName(personLastName.getText());
                        person.setFirstName(personFirstName.getText());
                        personTable = getPersonTable(person);
                        personTable.addMouseListener(mouseClicked);
                        scrollers = new JScrollPane (personTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                        constata.gridx = 3;
                        constata.gridy = 2;
                        constata.fill = GridBagConstraints.BOTH;
                        constata.insets = new Insets(10, 70, 20, 70);
                        constata.weightx = 10;
                        constata.weighty = 10;
                        panelCenter.add(scrollers,constata);
                        panelCenter.revalidate();
                        panelCenter.repaint();
                    }
                }
            }
        }

        private class KeyHandler extends KeyAdapter {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if(keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
                    if(!personId.getText().isEmpty()) {
                        Long id;
                        id = Long.parseLong(personId.getText());
                        panelCenter.remove(scrollers);
                        personTable = getPersonTable(id);
                        personTable.addMouseListener(mouseClicked);
                        scrollers = new JScrollPane (personTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                        constata.gridx = 3;
                        constata.gridy = 2;
                        constata.fill = GridBagConstraints.BOTH;
                        constata.insets = new Insets(10, 70, 20, 70);
                        constata.weightx = 10;
                        constata.weighty = 10;
                        panelCenter.add(scrollers,constata);
                        panelCenter.revalidate();
                        panelCenter.repaint();
                    }
                }
            }
        }

        private class HandlerClass implements ActionListener {
            public void actionPerformed(ActionEvent event) {
                Long id;  // will be used to get person by his ID
                if(event.getSource() == getPersonById) {
                    try {
                        if(!personId.getText().isEmpty()) {
                            id = Long.parseLong(personId.getText());
                            panelCenter.remove(scrollers);
                            personTable =  getPersonTable(id);
                            personTable.addMouseListener(mouseClicked);
                            scrollers = new JScrollPane (personTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                            constata.gridx = 3;
                            constata.gridy = 2;
                            constata.fill = GridBagConstraints.BOTH;
                            constata.insets = new Insets(10, 70, 20, 70);
                            constata.weightx = 10;
                            constata.weighty = 10;
                            panelCenter.add(scrollers,constata);
                            panelCenter.revalidate();
                            panelCenter.repaint();
                        }
                        else {
                            /*Filter by Last,First Names */
                            try{
                                panelCenter.remove(scrollers);
                                person.setLastName(personLastName.getText());
                                person.setFirstName(personFirstName.getText());
                                personTable = getPersonTable(person);
                                personTable.addMouseListener(mouseClicked);
                                scrollers = new JScrollPane (personTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                        JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                                constata.gridx = 3;
                                constata.gridy = 2;
                                constata.fill = GridBagConstraints.BOTH;
                                constata.insets = new Insets(10, 70, 20, 70);
                                constata.weightx = 10;
                                constata.weighty = 10;
                                panelCenter.add(scrollers,constata);
                                panelCenter.revalidate();
                                panelCenter.repaint();
                            }
                            catch(Exception ex){
                                System.err.println(ex);
                            }
                        }
                    }
                    catch(NumberFormatException ex){
                        JOptionPane.showMessageDialog(getContentPane(), "ID should be numeric symbol");
                        personId.setText(null);
                    }
                }else if(event.getSource() == addPersonButton) {
                    EditPersonDataGui addPerson = new EditPersonDataGui(null, personDao);
                    addPerson.editPersonDataFrame.setVisible(true);

                    panelCenter.remove(scrollers);
                    person.setLastName(personLastName.getText());
                    person.setFirstName(personFirstName.getText());
                    personTable =  getPersonTable(person);
                    personTable.addMouseListener(mouseClicked);
                    scrollers = new JScrollPane (personTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
                    constata.gridx = 3;
                    constata.gridy = 2;
                    constata.fill = GridBagConstraints.BOTH;
                    constata.insets = new Insets(10, 70, 20, 70);
                    constata.weightx = 10;
                    constata.weighty = 10;
                    panelCenter.add(scrollers,constata);
                    panelCenter.revalidate();
                    panelCenter.repaint();
                    }
                }

            }
        }




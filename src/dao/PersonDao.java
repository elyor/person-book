package dao;

import model.Person;
import util.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: RTulyaganov
 * Date: 8/8/12
 * Time: 11:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class PersonDao {
    private Connection con = ConnectionManager.getCon();
    private static final String TABLE_NAME = "test.names";
    private static final String LAST_NAME = "last_name";
    private static final String FIRST_NAME = "first_name";

    public Person getPersonById(Long id) throws SQLException {
        Person p = new Person();
        PreparedStatement pstate = con.prepareStatement("SELECT * FROM " + TABLE_NAME + " WHERE id = ?");
        pstate.setLong(1, id);
        ResultSet resultSet = pstate.executeQuery();
        while (resultSet.next()){
            p = parsePerson(resultSet);
        }
        return p;
    }

    public void addPerson(Person person) throws SQLException {
        PreparedStatement pstate = con.prepareStatement("INSERT INTO "+ TABLE_NAME + " ( " + LAST_NAME + ", " + FIRST_NAME + ")" +
                "VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
        pstate.setString(1, person.getLastName());
        pstate.setString(2, person.getFirstName());
        if(pstate.executeUpdate()==1){
            ResultSet result = pstate.getGeneratedKeys();
            result.next();
            person.setId(result.getLong(1));
        }
        pstate.close();
    }

    public void updatePerson(Person person) throws SQLException {
        PreparedStatement pstate = con.prepareStatement("UPDATE " + TABLE_NAME + " SET " + LAST_NAME + " = ?, " + FIRST_NAME + " = ? WHERE id = ?");
        pstate.setString(1, person.getLastName());
        pstate.setString(2, person.getFirstName());
        pstate.setLong(3, person.getId());
        pstate.executeUpdate();
        pstate.close();
    }

    public void deletePerson(Long id) throws SQLException {
        PreparedStatement pstate = con.prepareStatement("DELETE from " + TABLE_NAME + " WHERE id = ?");
        pstate.setLong(1, id);
        pstate.executeUpdate();
        pstate.close();
    }

    public List<Person> getPersons(Person person) throws SQLException, ObjectNotFoundException {
        List<Person> list = new ArrayList<Person>();
        PreparedStatement pstate = con.prepareStatement("SELECT * FROM " + TABLE_NAME + " WHERE " + LAST_NAME + " LIKE ? AND " + FIRST_NAME + " LIKE ?");
        pstate.setString(1, person.getLastName() + "%");
        pstate.setString(2, person.getFirstName() + "%");
        ResultSet resultset =  pstate.executeQuery();

        while (resultset.next()){
            person = parsePerson(resultset);
            list.add(person);
        }
        pstate.close();
        return list;
    }
    
    public List<Person> getPerson(Long id) throws SQLException {
        List<Person> list = new ArrayList<Person>();
        PreparedStatement pstate = con.prepareStatement("SELECT * FROM " + TABLE_NAME + " WHERE id = ?");
        pstate.setString(1, id.toString());
        ResultSet resultset = pstate.executeQuery();
        while (resultset.next()) {
            list.add(parsePerson(resultset));
        }
        pstate.close();
        return list;
    }

    private Person parsePerson(ResultSet resultSet) throws SQLException {
        Person p = new Person();
            p.setId(resultSet.getLong("id"));
            p.setLastName(resultSet.getString("last_name"));
            p.setFirstName(resultSet.getString("first_name"));
        return p;
    }
}

package util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by IntelliJ IDEA.
 * User: RTulyaganov
 * Date: 8/8/12
 * Time: 11:12 PM
 */
public class ConnectionManager {

    public static Connection getCon() {
        Connection createCon = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            createCon = DriverManager.getConnection("jdbc:mysql://localhost/test");
        } catch (Exception e) {
            System.err.println(e);
        }
        return createCon;
    }
}
